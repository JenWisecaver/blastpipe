# Divide and parallelize a large BLAST job using a PBS job array #

Number of jobs is user specified and dependent on the resources available. 

The **example/** directory contains E. cuniculi predicted proteins. Default values will blast Ecuniculi_pep.fa against itself using a five job array (JOBS=5). 

For a large all-vs-all blast of 50 or more genomes, I typically set JOBS=100. 

### USAGE ###

Create a working copy of the blastpipe directory 

```
cp -r blastpipe/ [working_dir]
```

Modify the following lines in the BlastPipeline.sh file as necessary
```
#!bash
QUERY="$CWD/example/Ecuniculi_pep.fa"		# path to query sequences in FASTA format		
JOBS=5						# number of jobs to split the query into
NAME='blast'					# name of job

# PBS variables
USER='jen.wisecaver@vanderbilt.edu'		# user email
NODES=1						# number of nodes requested
NTASKS=8					# number of cpus requested
TIME='96:0:0'					# maximum physical walltime 

# BLAST parameters
PROGRAM="blastp"             		# what flavor of blast do you want to run?
EVAL='1e-5'                  		# evalue threshold
DB="$CWD/example/Ecuniculi_pep" 	# path to database (eg nr, refseq, or custom) to blast against
OUTFMT=6		                # blast output format
MAXSEQS=1000				# maximum number of blast hits to include in output
```

Execute BlastPipeline.sh

```
./BlastPipeline.sh
```

### OUTPUT ###

The **split/** directory contains the query file divided into as many files as was specified using the JOBS variable in BlastPipeline.sh

The **blast-out/** directory contains the blast output, also divided.

Lastly, the **pbsout/** directory contains the PBS output and error files.


### CUSTOMIZATION ###

If necessary, additional parameters can be added to the BLAST command manually by editing the last line of the **run_blash.sh** file in the **scripts/** directory