#!/usr/bin/perl
use warnings;
use strict;

###############################################
# create_jobarray_fasta.pl 
# Jen Wisecaver
# 20141106 
# input: a fasta file 
# output: a directory with fasta files in jobarray
#         format 
################################################
 

my $file = $ARGV[0];
my $outdir = $ARGV[1];
my $jobs = $ARGV[2];  #number of jobs in the array 

# count number of sequences in infile 
my $total_sequences = 0;
open (FILE, "$file") || die "Cannot open indir fasta file: $file\n";
while (<FILE>) {
   if ($_ =~ /^>/) { $total_sequences++; }
}

# store the fasta file in hash 
my %seq_hash;
read_seqs($file,$jobs,$total_sequences);

# print split fasta files
foreach my $outfile ( keys %seq_hash ){
	open (OFIL, '>', $outfile) or die "Couldn't write to file $outfile: $!\n";
	
	foreach my $seq_name (keys %{$seq_hash{$outfile}} ){
		foreach my $sequence (keys %{$seq_hash{$outfile}{$seq_name}} ){
			print OFIL ">$seq_name\n$sequence";
		}
	}
	close OFIL;
}


sub read_seqs {
    my ($infile,$files_wanted,$total_sequences) = @_;
	open (IFIL, '<', $infile) or die "Couldn't open file $infile: $!\n";

    my $seq_per_file = int(($total_sequences / $files_wanted) + 1);
	my $file_counter = 1;
	my $current_seq_count = 0;
	my $total_seq_count = 0;
    my $filename = 'query';
    my $outfile = $outdir . '/' . $filename . '.' . $file_counter;

	$/ = ">";
	<IFIL>;
	while ( my $block = <IFIL> ) {
		$total_sequences++;

		$block =~ s/^([^\n]+)\n//;
		my $name = $1;
		my $seq = $block;
		$seq =~ s/>//g;

        $current_seq_count++;
        $total_seq_count++;

   	    if ($current_seq_count == $seq_per_file) {
        	$current_seq_count = 0;
            $file_counter++;
            $outfile = $outdir . '/' . $filename . '.' . $file_counter;
            
		}
        $seq_hash{$outfile}{$name}{$seq} = 1;
	}
	close IFIL;

}