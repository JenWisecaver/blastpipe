#!/bin/sh
### Request email when job begins and ends
#SBATCH --mail-type=ALL
#SBATCH --output="slurm-out/blast-%j-%a.out"

echo "SLURM_JOBID: " $SLURM_JOBID
echo "SLURM_ARRAY_TASK_ID: " $SLURM_ARRAY_TASK_ID
echo "SLURM_ARRAY_JOB_ID: " $SLURM_ARRAY_JOB_ID

# for blast
QUERY="$CWD/split/query.${SLURM_ARRAY_TASK_ID}" 
BOUTPUT="$BOUTDIR/blastout.${SLURM_ARRAY_TASK_ID}"

cd $CWD

echo submitting: $PROGRAM -query $QUERY -out $BOUTPUT -db $DB -evalue $EVAL -num_threads $NTASKS -max_target_seqs $MAXSEQS -outfmt $OUTFMT

time $PROGRAM -query $QUERY -out $BOUTPUT -db $DB -evalue $EVAL -num_threads $NTASKS -max_target_seqs $MAXSEQS -outfmt $OUTFMT
