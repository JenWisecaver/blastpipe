#!/bin/sh
export CWD=$PWD


### USER SPECIFIED VARIABLES ###
QUERY="$CWD/example/Ecuniculi_pep.fa"		# path to query sequences in FASTA format		
JOBS=5										# number of jobs to split the query into
NAME='blast'								# name of job

# PBS variables
USER='jen.wisecaver@vanderbilt.edu'		# user email
NODES=1								# number of cpus requested
NTASKS=8								# amount of resources that will be encumbered where cput=ncpus*walltime
TIME='96:0:0'							# maximum physical walltime 

# BLAST parameters
PROGRAM="blastp"             		# what flavor of blast do you want to run?
EVAL='1e-5'                  		# evalue threshold
DB="$CWD/example/Ecuniculi_pep" 	# path to database (eg nr, refseq, or custom) to blast against
OUTFMT=6		                	# blast output format
MAXSEQS=1000						# maximum number of blast hits to include in output


### Directory information ###
SPLITDIR="$CWD/split"
SCRIPTS="$CWD/scripts"
SLURMDIR="$CWD/slurm-out"
BOUTDIR="$CWD/blast-out"

echo "Creating working directories"
if [ ! -d "$SPLITDIR" ]; then mkdir $SPLITDIR; fi
if [ ! -d "$SLURMDIR" ]; then mkdir $SPLITDIR; fi
if [ ! -d "$BOUTDIR" ]; then mkdir $BOUTDIR; fi

## Preprocess fasta file
echo "Creating job array input files"
perl $SCRIPTS/create_jobarray_fasta.pl $QUERY $SPLITDIR $JOBS

## Submit blast job array
export QUERY PROGRAM EVAL DB OUTFMT CPU MAXSEQS BOUTDIR NTASKS
echo "Submitting blast job array -N $NAME -J 1-$JOBS $SCRIPTS/run_blast.sh"
FIRST=`sbatch --job-name=$NAME --mail-user=$USER --nodes=$NODES --ntasks=$NTASKS --time=$TIME --array=1-$JOBS $SCRIPTS/run_blast.sh`

echo "All jobs submitted."

